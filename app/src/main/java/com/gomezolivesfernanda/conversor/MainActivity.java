package com.gomezolivesfernanda.conversor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtNumero;
    Button btnConvertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNumero = (EditText) findViewById(R.id.txtNumero);
        btnConvertir = (Button) findViewById(R.id.btnConvertir);

        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Metodo que cambia de activity
                Intent intent = new Intent(getApplicationContext(), ResultadoActivity.class);
                //i es la variable donde se guarda
                float i = Float.parseFloat(txtNumero.getText().toString());
                //valor constante de la libra, convertido a float
                float valorLibra =  Float.parseFloat("2.20");
                //calculo
                float resultadoFloat = i*valorLibra;
                //conversion del resultado a una variable String
                String resultadoString = Float.toString(resultadoFloat);
                //contenedor donde se va a mandar el parametro a la siguiente activity
                Bundle bundle = new Bundle();
                //metodo putString para guardar parametro en el bundle, entre comillas clave y despues la variable
                bundle.putString("resultado", resultadoString);
                //metodo putExtras para enviar el parametro
                intent.putExtras(bundle);
                //Inicia la siguiente actividad
                startActivity(intent);
            }
        });
    }
}
