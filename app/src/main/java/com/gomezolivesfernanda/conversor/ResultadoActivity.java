package com.gomezolivesfernanda.conversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultadoActivity extends AppCompatActivity {

    TextView lblResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        //Se recibe el bundle
        Bundle bundle = this.getIntent().getExtras();

        //Guardar en variable string el parametro recibido en el bundle
        //se recibe por clave
        String resultado = bundle.getString("resultado");

        lblResultado = (TextView) findViewById(R.id.lblResultado);

        lblResultado.setText("El resultado es: " + resultado + "lbs");
    }
}
